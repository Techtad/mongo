var Socket = new io()

addEventListener("DOMContentLoaded", function (event) {
    Socket.on("refresh_resp", function (data) {
        console.log("refreshed", data.items)
        $("#data_display").text(JSON.stringify(data.items, null, 4))
        $("#selection").empty()
        for (let item of data.items) {
            let option = document.createElement("option")
            option.innerHTML = item._id
            $("#selection").append(option)
        }
    })
    Socket.on("insert_resp", function (data) {
        console.log("inserted", data)
    })
    Socket.on("update_resp", function (data) {
        console.log("updated", data)
    })
    Socket.on("delete_resp", function (data) {
        console.log("deleted", data)
    })

    $("#add_user").on("click", function (event) {
        let item = { user: $("#new_login").val(), pass: $("#new_password").val() }
        Socket.emit("insert", { item: item })
    })
    $("#refresh").on("click", function (event) {
        Socket.emit("refresh")
    })
    $("#delete").on("click", function (event) {
        Socket.emit("delete", { id: $("#selection").val() })
    })
    $("#update").on("click", function (event) {
        Socket.emit("update", { id: $("#selection").val(), item: { pass: $("#new_password").val() } })
    })

    Socket.emit("refresh")
})