var http = require("http")
var fs = require("fs")
var SocketIO = require("socket.io")
var mongodb = require("mongodb")
var MongoClient = mongodb.MongoClient
var ObjectID = mongodb.ObjectID
var Operations = require("./modules/operations.js")
var ops = new Operations()
var _db

function sendFile(fileName, res) {
    if (fileName == "/") fileName = "/static/index.html"
    fs.readFile(`${__dirname}/${fileName}`, function (error, data) {
        if (error) {
            console.log(error)
            res.writeHead(404, { "Content-Type": "text/html;charset=utf-8" })
            res.end(`Błąd 404: Nie znaleziono pliku ${fileName}`)
        } else {
            let nameParts = fileName.split(".")
            let ext = nameParts[nameParts.length - 1].toLowerCase()
            let contentType = "text/html;charset=utf-8"
            switch (ext) {
                case "js":
                    contentType = "application/javascript"
                    break
                case "json":
                    contentType = "application/json"
                    break
                case "css":
                    contentType = "text/css"
                    break
                case "jpg": case "jpeg":
                    contentType = "image/jpeg"
                    break
                case "mp3":
                    contentType = "audio/mpeg"
                    break
                case "png":
                    contentType = "image/png"
                    break
                case "svg":
                    contentType = "image/svg+xml"
                    break
                case "gif":
                    contentType = "image/gif"
                    break
                case "txt":
                    contentType = "text/plain"
                    break
                default: break
            }
            res.writeHead(200, { "Content-Type": contentType })
            res.end(data)
        }
    })
}

function postRepsonse(req, res) {
    let allData = ""
    req.on("data", function (data) {
        allData += data
    })
    req.on("end", function (data) {
        console.log("Data(" + req.url + "):" + allData)
        let obj
        if (allData) {
            try {
                obj = JSON.parse(allData)
            } catch (error) {
                console.log(error)
                res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                res.end(error)
            }
        }
        switch (req.url) {
            default:
                console.log("Błędny POST: " + req.url)
                res.writeHead(400, { "Content-Type": "application/json;charset=utf-8" })
                res.end("Błędna akcja POST: " + req.url)
                break
        }
    })
}

var server = http.createServer(function (req, res) {
    console.log(req.method + " : " + req.url)
    switch (req.method) {
        case "GET":
            sendFile(decodeURI(req.url).split('?')[0], res)
            break;
        case "POST":
            postRepsonse(req, res)
            break;
    }
})

const port = 3000
server.listen(3000, function () {
    console.log("Start serwera na porcie " + port)
})

var MyColl

MongoClient.connect("mongodb://localhost/3ib1", function (err, db) {
    if (err) console.log(err)
    else console.log("mongo podłączone")
    _db = db;
    MyColl = db.collection("crud")
    console.log(MyColl)
})

var SocketServer = SocketIO.listen(server)
console.log("Start Socket.io")

SocketServer.on("connection", function (client) {
    console.log("Klient się połączył:", client.id)
    client.on("disconnect", function () {
        console.log("Klient się rozłączył:", this.id)
    })

    client.on("refresh", function (data) {
        ops.SelectAll(MyColl, function (items) {
            this.emit("refresh_resp", { items: items })
        }.bind(this))
    })
    client.on("insert", function (data) {
        ops.Insert(MyColl, data.item, function (result) {
            this.emit("insert_resp", { result: result })
        }.bind(this))
    })
    client.on("update", function (data) {
        ops.UpdateById(ObjectID, MyColl, data.id, data.item, function (data) {
            this.emit("update_resp", { data: data })
        }.bind(this))
    })
    client.on("delete", function (data) {
        ops.SelectAll(ObjectID, MyColl, data.id, function (data) {
            this.emit("delete_resp", { data: data })
        }.bind(this))
    })
})